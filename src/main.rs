#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate rocket;

#[macro_use]
extern crate lazy_static;

use regex::Regex;
use rocket::http::Status;
use rocket::response::content::Html;
use rocket::response::Redirect;
use serde_json::Value;
use std::io::Read;

lazy_static! {
    static ref ID: Regex = Regex::new(r".*(om\d+)(\.aspx)?").expect("Regex is valid");
}
#[get("/news?<url>")]
fn news(url: String) -> Result<Html<String>, Status> {
    match ID.captures(&url) {
        Some(captures) => {
            let mut url = "https://corren.se/ws/GetArticle.ashx?articleid=".to_owned();
            url.push_str(&captures[1]);
            let mut res = reqwest::get(url.as_str()).map_err(|_| Status::UnprocessableEntity)?;
            let mut body = String::new();
            res.read_to_string(&mut body)
                .map_err(|_| Status::UnprocessableEntity)?;

            let html: Value =
                serde_json::from_str(body.as_str()).map_err(|_| Status::UnprocessableEntity)?;
            let content = html["d"]["html"].to_string();
            let content = content.replace("\\r", "");
            let content = content.replace("\\n", "");
            let content = content.replace("\\t", "");
            let content = content.replace("\\\"", "\"");
            let content = content.replace("<div class=\"swiper-loading\" style=\"height: 551px;\">        <img src=\"/public/img/dialog/ajax-loader.gif\" />    </div>", "");
            let content = content.replace("\"<div", "<div");
            let content = content.replace("div>\"", "div>");
            Ok(Html(content))
        }
        None => Err(Status::UnprocessableEntity),
    }
}

#[get("/news/shorten?<url>")]
fn shorten(url: String) -> Result<Redirect, Status> {
    match ID.captures(&url) {
        Some(captures) => Ok(Redirect::to(uri!(news: String::from(&captures[1])))),
        None => Err(Status::UnprocessableEntity),
    }
}

#[get("/news")]
fn news_input() -> Html<String> {
    Html("<div style=\"height: 100%; display: flex; align-items: center; justify-content: center;\"><form action=\"/news/shorten\" target=\"_self\" method=\"get\"><input type=\"text\" name=\"url\"></form></div>".to_string())
}

fn main() {
    rocket::ignite()
        .mount("/", routes![news, news_input, shorten])
        .launch();
}
